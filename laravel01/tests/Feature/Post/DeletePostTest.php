<?php

namespace Tests\Feature\Post;

use App\Models\Post;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class DeletePostTest extends TestCase
{
    /** @test */
    public function user_can_delete_post_if_post_exists()
    {
        $post = Post::factory()->create();
        $response = $this->deleteJson(route('posts.destroy', $post->id));
        $response->assertStatus(Response::HTTP_OK);

        $response->assertJson(
            fn (AssertableJson $json) =>
            $json->has(
                'data',
                fn (AssertableJson $json) =>
                $json->where('name', $post->name)
                    ->etc()
            )
                ->has('message')
        );
    }
    /** @test */

    public function user_can_not_delete_post_if_post_not_exists()
    {
        $postId = -1;
        $response = $this->deleteJson(route('posts.destroy', $postId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
