<?php

namespace Tests\Feature\Post;

use App\Models\Post;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class UpdatePostTest extends TestCase
{
    /** @test */

    public function user_can_update_post_if_data_is_valid()
    {
        $post = Post::factory()->create();
        $dataUpdate = [
            'name' => $this->faker->name,
            'body' => $this->faker->text
        ];

        $response = $this->putJson(route('posts.update', $post->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_OK);

        $response->assertJson(
            fn (AssertableJson $json) =>
            $json->has(
                'data',
                fn (AssertableJson $json) =>
                $json->where('name', $dataUpdate['name'])
                ->where('body', $dataUpdate['body'])
                ->etc()
            )
                ->has('message')
                ->etc()
        );

        $this->assertDatabaseHas('posts', [
            'name' => $dataUpdate['name'],
            'body' => $dataUpdate['body']
        ]);

    }
    /** @test */

    public function user_can_not_update_post_if_name_is_null()
    {
        $post = Post::factory()->create();
        $dataUpdate = [
            'name' => '',
            'body' => $this->faker->text
        ];

        $response = $this->putJson(route('posts.update', $post->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $response->assertJson(
            fn (AssertableJson $json) =>
            $json->has(
                'errors',
                fn (AssertableJson $json) =>
                $json->has('name')
                ->etc()
            )
                ->has('message')
                ->etc()
        );
    }
    /** @test */

    public function user_can_not_update_post_if_body_is_null()
    {
        $post = Post::factory()->create();
        $dataUpdate = [
            'name' => $this->faker->name,
            'body' => ''
        ];

        $response = $this->putJson(route('posts.update', $post->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $response->assertJson(
            fn (AssertableJson $json) =>
            $json->has(
                'errors',
                fn (AssertableJson $json) =>
                $json->has('body')
                ->etc()
            )
                ->has('message')
                ->etc()
        );
    }
    /** @test */

    public function user_can_not_update_post_if_nor_exists_and_data_is_not_valid()
    {
        $postId = -1;
        $dataUpdate = [
            'name' => '',
            'body' => ''
        ];

        $response = $this->putJson(route('posts.update', $postId, $dataUpdate));
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $response->assertJson(
            fn (AssertableJson $json) =>
            $json->has(
                'errors',
                fn (AssertableJson $json) =>
                $json->has('name')
                ->has('body')
                ->etc()
            )
                ->has('message')
                ->etc()
        );
    }
}
